package org.blocklaunch;

public class Mod{
	
	public String name;
	public boolean active = true;
	public String version;
	
	public String getName() {
		return name;
	}

	public boolean isActive() {
		return active;
	}

	public String getVersion() {
		return version;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Mod(String modname, String modversion, boolean isactive){
		this.name = modname;
		this.version = modversion;
		this.active = isactive;
	}
	
	public Mod(String modname, String modversion){
		this.name = modname;
		this.version = modversion;
		this.active = true;
	}
	
	@Override
	public String toString(){
		String ret = name+" Version: "+version+":";
		if(active){ret+="enabled";}else{ret+="disabled";};
		return ret;
	}
}
