package org.blocklaunch;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipException;

import jupar.Downloader;

public class ModPack {
	public static ModPack modpack = new ModPack();
	
	private List<Mod> modlist = new ArrayList<Mod>();
	
	/**
	 * initialises Modlist
	 */
	public ModPack(){
		if(modlist.isEmpty()){
			initModlist();
		}
	}
	
	public void setModActive(Mod mod, boolean active){
		if(modlist.isEmpty()){
			initModlist();
		}
		for(int i= 0; i<modlist.size(); i++){
			if(mod.equals(modlist.get(i))){
				modlist.get(i).setActive(active);
			}
		}
	}
	
	/**
	 * initializes Modlist from BlockLaunch_Pack-x.x.x-y.y.y.jar (MC [x] and BL[y]-Version)
	 * listing Mods and their versions based on Filename ([modname]-[mcversion]-[modversion]);
	 */
	public void initModlist(){
		String filename = "BlockLaunch_Pack-"+Data.VERSION+".jar";
		URL modpackurl = ModPack.class.getResource("/"+filename);
		File modpack_jar = new File(filename);
		Downloader dl = new Downloader();
		JarFile modpack = null;
		try{
			dl.wget(modpackurl, filename);
			modpack = new JarFile(modpack_jar);
			Enumeration<JarEntry> entries = modpack.entries();
			
			JarEntry modpack_entry;
			while(entries.hasMoreElements()){
				modpack_entry = entries.nextElement();
				if(modpack_entry != null){
					String name = modpack_entry.getName();
					name = name.substring(0, name.length()-4);
					String[] nameparts = name.split("-");
					if((nameparts[0]!= null)&&(nameparts[2]!= null)){
						Mod newmod = new Mod(nameparts[0], nameparts[2]);
						modlist.add(newmod);
					}	
				}
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			modpack_jar.delete();
			try {
				modpack.close();
			} catch (IOException e) {
				e.printStackTrace();
			}			
		}
	}
	/**
	 * Installs the Modpack as whole jar at the given dir
	 * @param packdir the place where the Modpack will be installed
	 * @return sucess of installation
	 */
	public boolean install(File packdir){
		Mod[] mods = getActiveMods();
		File dir = new File(packdir, "temp");
		if(!dir.exists()){dir.mkdir();};
		String outputName = "BlockLaunch-"+Data.VERSION+".jar";
		String modpackname = "BlockLaunch_Pack-"+Data.VERSION+".jar";
		File tempfile = new File(dir, modpackname);
		File basemod = new File(dir, outputName);
		//dateien extrahieren		
		try{
			tempfile.createNewFile();
			basemod.createNewFile();		
			Downloader dl = new Downloader();
			dl.wget(ModPack.class.getResource("/"+outputName), basemod.getAbsolutePath());
			dl.wget(ModPack.class.getResource("/"+modpackname), tempfile.getAbsolutePath());
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		JarFile temp = null;
		try{
			temp = new JarFile(tempfile);
			Enumeration<JarEntry> entries = temp.entries();
			JarEntry entry = null;
			while(entries.hasMoreElements()){
				entry = entries.nextElement();
				if(entry != null){
					for(int i = 0; i<mods.length; i++){
						//nur aktivierte mods entpacken
						if(entry.getName().startsWith(mods[i].getName())){
							System.out.println("Install "+ entry.getName());
							BufferedOutputStream zos = null;
							try{
								File mod = new File(dir, entry.getName());
								zos = new BufferedOutputStream(new FileOutputStream(mod));
								copy(temp.getInputStream(entry), zos);
							}finally{
								zos.close();
							}							
						}						
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			try {
				temp.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			tempfile.delete();
			basemod.delete();
		}
		//files zusammenfassen
		File output = new File(packdir, outputName);
		JarFile modjar = null;
		JarOutputStream out = null;
		File[] modfiles = dir.listFiles();
		try{
			output.createNewFile();
			out = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(output)));
			
			for(int i = 0; i<modfiles.length; i++){			
				modjar = new JarFile(modfiles[i]);
				Enumeration<JarEntry> entries = modjar.entries();
				while(entries.hasMoreElements()){
					JarEntry entry = entries.nextElement();
					String name = entry.getName();
					if(!(name.startsWith("META-INF")||name.startsWith("mcmod")||name.startsWith("Read"))){
						if(!entry.isDirectory()){
							try{
								out.putNextEntry(new JarEntry(entry.getName()));
								copy(modjar.getInputStream(entry), out);
							}catch(ZipException e){}
						}
					}
					out.closeEntry();
				}
				modjar.close();
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			try {
				out.close();
				modjar.close();				
				deleteDirectory(dir);
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}				
		}		
		return true;
	}
	
	public Mod[] getModlist(){
		if(modlist.isEmpty()){
			initModlist();
		}
		//return (Mod[]) modlist.toArray();
		return convertListToArray(modlist);
	}
	
	public Mod[] getActiveMods(){
		ArrayList<Mod> activemods = new ArrayList<Mod>();
		for(int i = 0; i<modlist.size(); i++){
			if(modlist.get(i).isActive()){
				activemods.add(modlist.get(i));
			}
		}
		//return (Mod[]) activemods.toArray();
		return convertListToArray(activemods);
	}
	
	public static void copy(InputStream input, OutputStream output)
			throws IOException {
//		System.out.println(ByteStreams.copy(input, output)+" Bytes coped");
		byte[] BUFFER = new byte[4096 * 1024];
		int bytesRead;
		while ((bytesRead = input.read(BUFFER)) != -1) {
			output.write(BUFFER, 0, bytesRead);
		}
	}
	
	public static Mod[] convertListToArray(List<Mod> modlist){
		Mod[] array = new Mod[modlist.size()];
		for(int i = 0; i<modlist.size(); i++){
			array[i] = modlist.get(i);
		}		
		return array;
	}
	
	public static boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	
}
