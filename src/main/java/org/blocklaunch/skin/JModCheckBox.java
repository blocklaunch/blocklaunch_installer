package org.blocklaunch.skin;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import org.blocklaunch.Mod;
import org.blocklaunch.ModPack;

public class JModCheckBox extends JCheckBox {
	static final long serialVersionUID = 1L;
	
	public JModCheckBox(final Mod mod){
		super(renameName(mod.getName()), mod.isActive());
		this.setToolTipText("Version: "+mod.getVersion());
		this.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent ie) {
//					mod.setActive();
					ModPack.modpack.setModActive(mod, isSelected());
			}});
	}
	
	private static String renameName(String name){
		String[] newname =name.split("_");
		String ret = "";
		if(newname.length>1){
			for(int i = 0; i<newname.length-1; i++){
				ret = ret + newname[i]+" ";
			}
			ret = ret+newname[newname.length-1];
		}else{
			ret = name;
		}
		return ret;
	}

}
