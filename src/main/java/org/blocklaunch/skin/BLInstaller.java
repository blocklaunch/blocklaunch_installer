package org.blocklaunch.skin;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import net.minecraftforge.installer.InstallerAction;
import net.minecraftforge.installer.SimpleInstaller;
import net.minecraftforge.installer.VersionInfo;

import org.blocklaunch.Data;
import org.blocklaunch.Mod;
import org.blocklaunch.ModPack;
import org.blocklaunch.util.Util;

import com.google.common.base.Throwables;

public class BLInstaller implements ActionListener{
	public JFrame frame;
	File targetDir;
	
	static Color background = new Color(255,255,255);
	static Color green = new Color(139, 195, 74);
	static Color clicked_green = new Color(210, 204, 74);
	static Color gray = new Color(238,238,238);
	
	private String ACTION_CLIENT = "client";
	private String ACTION_SERVER = "server";
	private String ACTION_EXTRACT = "extract";
	private String ACTION_HOMEPAGE = "homepage";
	private String ACTION_CREDITS = "credits";	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) throws IOException {		
            EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BLInstaller window = new BLInstaller();
					window.frame.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});  
	}

	/**
	 * Create the application.
	 */
	public BLInstaller() {		
		
		String userHomeDir = System.getProperty("user.home", ".");
        String osType = System.getProperty("os.name").toLowerCase(Locale.ENGLISH);
        targetDir = null;
        String mcDir = ".minecraft";
        if (osType.contains("win") && System.getenv("APPDATA") != null)
        {
            targetDir = new File(System.getenv("APPDATA"), mcDir);
        }
        else if (osType.contains("mac"))
        {
            targetDir = new File(new File(new File(userHomeDir, "Library"),"Application Support"),"minecraft");
        }
        else
        {
            targetDir = new File(userHomeDir, mcDir);
        }

        String path = VersionInfo.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        if (path.contains("!/"))
        {
            JOptionPane.showMessageDialog(null, "Due to java limitation, please do not run this jar in a folder ending with ! : \n"+ path, "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try
        {
            VersionInfo.getVersionTarget();
        }
        catch (Throwable e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Corrupt download detected, cannot install", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
		
		
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		BufferedImage image, windowicon;
        try
        {
            image = ImageIO.read(SimpleInstaller.class.getResourceAsStream("/org/blocklaunch/skin/Logo.png"));
            windowicon = ImageIO.read(SimpleInstaller.class.getResourceAsStream("/org/blocklaunch/skin/Icon.png"));
            
        }
        catch (IOException e)
        {
            throw Throwables.propagate(e);
        }
    	ImageIcon icon = new ImageIcon(image);
		
    	try {
    		NimbusLookAndFeel laf = new NimbusLookAndFeel();
    		 //laf.load(BLInstaller.class.getResourceAsStream("laf.xml"), BLInstaller.class);
			UIManager.setLookAndFeel(laf);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
		frame = new JFrame();
		frame.setTitle("BlockLaunch Installer (BlockLaunch "+Data.BLVERSION+")");
		frame.setIconImage(windowicon);
		
		frame.setSize(672, 370);
		frame.setMinimumSize(frame.getSize());
		frame.setBackground(background);
		frame.setLocationRelativeTo(null);
//		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		
		JPanel titlePanel = new JPanel();
		titlePanel.setBackground(background);
		JLabel logo = new JLabel(icon);
		logo.setOpaque(true);	
		logo.setBackground(background);
//		titlePanel.setSize(image.getWidth(), image.getHeight()+10);
		titlePanel.setPreferredSize(new Dimension(frame.getWidth(), image.getHeight()+10));
		titlePanel.setMinimumSize(titlePanel.getSize());
		titlePanel.add(logo);			
		//System.out.println(titlePanel.getSize().toString());
		frame.getContentPane().add(titlePanel);
		
		JPanel modTitlePanel = new JPanel();
		modTitlePanel.setBackground(green);
		JLabel modstext = new JLabel("Mods");
		modstext.setFont(new Font("Tahoma", Font.BOLD, 25));
		modstext.setOpaque(true);
		modstext.setForeground(background);
		modstext.setBackground(modTitlePanel.getBackground());
		modTitlePanel.add(modstext);
		modTitlePanel.setSize(frame.getWidth(), modstext.getHeight()+10);
		modTitlePanel.setPreferredSize(new Dimension(frame.getWidth(), 40));
		//modTitlePanel.setMaximumSize(modTitlePanel.getPreferredSize());
		
		frame.getContentPane().add(modTitlePanel);
		
		JPanel modsPanel = new JPanel();
		modsPanel.setBackground(background);
		frame.getContentPane().add(modsPanel);			
		Mod[] modlist = ModPack.modpack.getModlist();
		
		int rows = (int) Math.ceil(modlist.length/3);
		int rowheight = 25;
		//height-anpassung?
		if(rows <= 5){
			rows = 5;
			modsPanel.setPreferredSize(new Dimension(frame.getWidth()-10, 5*rowheight));
			modsPanel.setAlignmentX((float) 0.5);
		}else{
			modsPanel.setPreferredSize(new Dimension(frame.getWidth()-10, rows*rowheight));
			modsPanel.setAlignmentX((float) 0.5);
		}
		modsPanel.setLayout(new GridLayout((rows<5)?5:rows, 3, 5, 1));
		
        for(int i = 0; i<modlist.length; i++){
        	JModCheckBox modcheck = new JModCheckBox(modlist[i]); 
//        	System.out.println(modcheck.getHeight());
        	modcheck.setBackground(modsPanel.getBackground());
//        	modcheck.setAlignmentX(LEFT_ALIGNMENT);
//        	modcheck.setAlignmentY(CENTER_ALIGNMENT);
        	modsPanel.add(modcheck);        	
        }
        
		
		JPanel infoArea = new JPanel();
		infoArea.setBackground(gray);
		frame.getContentPane().add(infoArea);
		infoArea.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		Dimension buttonSize = new Dimension(150, 25);
		
		if(!VersionInfo.hideClient()){
		ColoredButton clientInstall = new ColoredButton("Install Client", true);
		clientInstall.addActionListener(this);
		clientInstall.setActionCommand(ACTION_CLIENT);
		clientInstall.setMinimumSize(buttonSize);
		clientInstall.setPreferredSize(clientInstall.getMinimumSize());
		infoArea.add(clientInstall);
		}
		
		if(!VersionInfo.hideServer()){
		ColoredButton serverInstall = new ColoredButton("Install Server");
		serverInstall.addActionListener(this);
		serverInstall.setActionCommand(ACTION_SERVER);
		serverInstall.setMinimumSize(buttonSize);
		serverInstall.setPreferredSize(serverInstall.getMinimumSize());
		infoArea.add(serverInstall);
		}
		
		ColoredButton extractInstall = new ColoredButton("Extract");
		extractInstall.addActionListener(this);
		extractInstall.setActionCommand(ACTION_EXTRACT);
		extractInstall.setMinimumSize(new Dimension(80, buttonSize.height));
		extractInstall.setPreferredSize(extractInstall.getMinimumSize());
		infoArea.add(extractInstall);
		
		ColoredButton homepage = new ColoredButton("Homepage");
		homepage.addActionListener(this);
		homepage.setActionCommand(ACTION_HOMEPAGE);
		homepage.setMinimumSize(new Dimension(100,buttonSize.height));
		homepage.setPreferredSize(homepage.getMinimumSize());
		infoArea.add(homepage);
		
		ColoredButton credits = new ColoredButton("?");
		credits.addActionListener(this);
		credits.setActionCommand(ACTION_CREDITS);
		credits.setMinimumSize(new Dimension(buttonSize.height, buttonSize.height));
		credits.setPreferredSize(credits.getMinimumSize());
		infoArea.add(credits);
		
		infoArea.repaint();
		infoArea.setPreferredSize(new Dimension(frame.getWidth(), 35));
		//infoArea.setMaximumSize(infoArea.getPreferredSize());
		
		frame.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		File installDir;
		if(e.getActionCommand().equals(ACTION_CREDITS)){
			Credits credits = new Credits();
			credits.setVisible(true);
		}
		if(e.getActionCommand().equals(ACTION_HOMEPAGE)){
			try {
				Util.openLink(new URI("http://www.blocklaunch.net"));
			} catch (URISyntaxException ex) {
				ex.printStackTrace();
			}
		}
		if(e.getActionCommand().equals(ACTION_CLIENT)){
			InstallerAction action = InstallerAction.CLIENT;
			if(action.run(targetDir)){
				BLDialog dialog = new BLDialog(action.getSuccessMessage());
				dialog.setAlwaysOnTop(true);
				dialog.setVisible(true);
			}
		}
		if(e.getActionCommand().equals(ACTION_SERVER)){
			JFileChooser dirChooser = new JFileChooser();
			dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            dirChooser.setFileHidingEnabled(false);
//            dirChooser.ensureFileIsVisible(targetDir);
            dirChooser.setSelectedFile(null);
            int response = dirChooser.showSaveDialog(frame);
            switch (response)
            {
            case JFileChooser.APPROVE_OPTION:
                installDir = dirChooser.getSelectedFile();
                //updateFilePath();
                InstallerAction action = InstallerAction.SERVER;
                if(action.run(installDir)){
                	BLDialog dialog = new BLDialog(action.getSuccessMessage());
    				dialog.setAlwaysOnTop(true);
                	dialog.setVisible(true);
                }
                break;
            default:
                break;
            }
			
		}
		if(e.getActionCommand().equals(ACTION_EXTRACT)){
			JFileChooser dirChooser = new JFileChooser();
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            dirChooser.setFileHidingEnabled(false);
//            dirChooser.ensureFileIsVisible(targetDir);
            dirChooser.setSelectedFile(null);
            int response = dirChooser.showSaveDialog(frame);
            switch (response)
            {
            case JFileChooser.APPROVE_OPTION:
                installDir = dirChooser.getSelectedFile();
                //updateFilePath();
                InstallerAction action = InstallerAction.EXTRACT;
                if(action.run(installDir)){
                	BLDialog dialog = new BLDialog(action.getSuccessMessage());
    				dialog.setAlwaysOnTop(true);
                	dialog.setVisible(true);
                }
                break;
            default:
                break;
            }
			
		}		
	}	
}