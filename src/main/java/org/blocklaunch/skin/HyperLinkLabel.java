package org.blocklaunch.skin;

import java.awt.Label;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;

import org.blocklaunch.util.Util;

public class HyperLinkLabel extends Label implements MouseListener {
	private static final long CLICK_DELAY = 250L;
	private static final long serialVersionUID = 1L;
	private String url;
	private long lastClick = System.currentTimeMillis();
	
	public HyperLinkLabel(String text, String url){
		super(text);
		this.url = url;
		super.addMouseListener(this);
//		super.set
//		super.set
//		setToolTip("Click to visit");
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		if(lastClick+CLICK_DELAY > System.currentTimeMillis()){
			return;
		}
		lastClick = System.currentTimeMillis();
		try {
			Util.openLink(new URI(url));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
