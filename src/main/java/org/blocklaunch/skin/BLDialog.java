package org.blocklaunch.skin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class BLDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			BLDialog dialog = new BLDialog("nul");
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			dialog.setAlwaysOnTop(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	Font font = new Font(ColoredButton.font.getFontName(), Font.PLAIN, ColoredButton.font.getSize()-6);	
	Font font_link = new Font(ColoredButton.font.getFontName(), Font.ITALIC, ColoredButton.font.getSize()-6);	
	Color color = new Color(235,245,245);

	/**
	 * Create the dialog.
	 */
	public BLDialog(String text) {
		Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
		fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		font_link.deriveFont(fontAttributes);
		
		
		setSize(200, 100);
		setUndecorated(true);
		setLocationRelativeTo(null);
//		setBounds(100, 100, 450, 300);
		setBackground(color);		
		getContentPane().setLayout(new BorderLayout());		
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setBackground(color);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		contentPanel.setAlignmentY(Label.CENTER);
		{
			Label label = new Label(text);
			label.setFont(font);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			buttonPane.setBackground(getBackground());
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				ColoredButton okButton = new ColoredButton("OK", true);
				okButton.setPreferredSize(new Dimension(70, 25));
				okButton.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		pack();
	}

}
