package org.blocklaunch.skin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Credits extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Credits dialog = new Credits();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	Font font = new Font(ColoredButton.font.getFontName(), Font.PLAIN, ColoredButton.font.getSize()-6);	
	Font font_link = new Font(ColoredButton.font.getFontName(), Font.ITALIC, ColoredButton.font.getSize()-6);	
	Color color = new Color(235,245,245);

	/**
	 * Create the dialog.
	 */
	public Credits() {
		Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
		fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		font_link.deriveFont(fontAttributes);
		
		
		setTitle("Credits");
		setSize(300, 200);
		setUndecorated(true);
		setLocationRelativeTo(null);
//		setBounds(100, 100, 450, 300);
		setBackground(color);		
		getContentPane().setLayout(new BorderLayout());		
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setBackground(color);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
		contentPanel.setAlignmentY(Label.CENTER);
		{
			Label label = new Label("Credits");
			label.setAlignment(Label.CENTER);
			Font font2 = new Font(ColoredButton.font.getFontName(), Font.BOLD, ColoredButton.font.getSize());
			label.setFont(font2);
			contentPanel.add(label);
		}
		{
			Label label = new Label("Mods and Modpack by the BlockLaunch Team");
			label.setFont(font);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		{
			Label label = new Label("BlockLaunch Team is a part of Team Melanistic");
			label.setFont(font);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		{
			HyperLinkLabel label = new HyperLinkLabel("www.team-melanistic.com", "http://www.team-melanistic.com");
			label.setFont(font_link);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		{
			Label label = new Label("Minecraft Forge by the Minecraft Forge Team");
			label.setFont(font);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		{
			HyperLinkLabel label = new HyperLinkLabel("www.minecraftforge.net", "http://www.minecraftforge.net");
			label.setFont(font_link);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		{
			Label label = new Label("Installer based on Minecraft Forge Installer");
			label.setFont(font);
			label.setAlignment(Label.CENTER);
			contentPanel.add(label);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			buttonPane.setBackground(getBackground());
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				ColoredButton okButton = new ColoredButton("OK", true);
				okButton.setPreferredSize(new Dimension(70, 25));
				okButton.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		pack();
	}

}
