package org.blocklaunch;


public class Data {

	public static final String MCVERSION = "1.7.10";

	public static final String BLVERSION = "1.0";
	/**
	 * Version of the Modpack formatted: MCVERSION-BLVERSION
	 */
	public static final String VERSION = MCVERSION+"-"+BLVERSION;
		
	public static final String URL = "www.blocklaunch.net";

	public static final String SERVER = "http://dl.blocklaunch.net/";
	
	public static final String UpdateSite = "http://forum.blocklaunch.net/index.php";

	public static final String Java_update = "http://www.java.com";

	public static final String officiallauncher_DL = "http://www.minecraft.net";
	

}
