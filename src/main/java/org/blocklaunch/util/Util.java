package org.blocklaunch.util;

import java.io.File;
import java.lang.reflect.Array;
import java.net.URI;
import java.util.ArrayList;

public class Util {
	
	public static <T> T[] convertListToArray(Class<T> classOfList, ArrayList<T> list){
		@SuppressWarnings("unchecked")
		T[] array = (T[]) Array.newInstance(classOfList, list.size());
		for(int i = 0; i<list.size(); i++){
			array[i] = list.get(i);
		}
		
		return array;
	}
	
	public static void openLink(URI paramURI) {
		try {
			Object localObject = Class.forName("java.awt.Desktop")
					.getMethod("getDesktop", new Class[0])
					.invoke(null, new Object[0]);
			localObject.getClass()
					.getMethod("browse", new Class[] { URI.class })
					.invoke(localObject, new Object[] { paramURI });
		} catch (Throwable localThrowable) {
			System.out.println("Failed to open link " + paramURI.toString());
		}
	}
	

    public static OS getPlatform() {
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.contains("win")) return OS.WINDOWS;
        if (osName.contains("mac")) return OS.MACOS;
        if (osName.contains("linux")) return OS.LINUX;
        if (osName.contains("unix")) return OS.LINUX;
        return OS.UNKNOWN;
    }

    public static File getStandardWorkingDirectory() {
        String userHome = System.getProperty("user.home", ".");
        File workingDirectory;
        OS os = getPlatform();
        System.out.println(os.ordinal());
        if (os == OS.LINUX)
        	{
            workingDirectory = new File(userHome, ".blocklaunch/");
        	}
        else{
        	if (os == OS.WINDOWS)
        	{
        		String applicationData = System.getenv("APPDATA");
        		String folder = applicationData != null ? applicationData : userHome;
        		workingDirectory = new File(folder, ".blocklaunch/");
        	}else{
        		if (os == OS.MACOS){
        			workingDirectory = new File(userHome, "Library/Application Support/blocklaunch");
        		}else{
        			workingDirectory = new File(userHome, "blocklaunch/");}
        		}
        	} 
        
        return workingDirectory;
    }


    public static enum OS {
        WINDOWS, MACOS, SOLARIS, LINUX, UNKNOWN;
    }

}
