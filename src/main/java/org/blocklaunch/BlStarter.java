package org.blocklaunch;

import java.awt.EventQueue;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpecBuilder;
import net.minecraftforge.installer.InstallerAction;
import net.minecraftforge.installer.ServerInstall;
import net.minecraftforge.installer.VersionInfo;

import org.blocklaunch.skin.BLInstaller;

public class BlStarter {
	
		/**
		 * Launch the application.
		 * @throws IOException 
		 */
		public static void main(String[] args) throws IOException {
			setupLogger();
	        OptionParser parser = new OptionParser();
	        OptionSpecBuilder serverInstallOption = parser.accepts("installServer", "Install a server to the current directory");
	        OptionSpecBuilder extractOption = parser.accepts("extract", "Extract the contained jar file");
	        OptionSpecBuilder helpOption = parser.acceptsAll(Arrays.asList("h", "help"),"Help with this installer");
	        OptionSet optionSet = parser.parse(args);

	        if (optionSet.specs().size()>0)
	        {	        	
	            handleOptions(parser, optionSet, serverInstallOption, extractOption, helpOption);
	        }
	        else
	        {
	            EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						BLInstaller window = new BLInstaller();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
	        }
			
			
		}

		
		
		private static void setupLogger()
	    {
	        File f = new File(VersionInfo.class.getProtectionDomain().getCodeSource().getLocation().getFile());
	        File output;
	        if (f.isFile()) output = new File(f.getName() + ".log");
	        else            output = new File("installer.log");

	        try
	        {
	            System.out.println("Setting up logger: " + output.getAbsolutePath());
	            OutputStream fout = new BufferedOutputStream(new FileOutputStream(output));
	            System.setOut(new PrintStream(new MultiOutputStream(System.out, fout), true));
	            System.setErr(new PrintStream(new MultiOutputStream(System.err, fout), true));
	        }
	        catch (FileNotFoundException e)
	        {
	            e.printStackTrace();
	            //We errored out, lets just continue on and hope the rest runs fine.
	        }
	    }
		
		private static void handleOptions(OptionParser parser, OptionSet optionSet, OptionSpecBuilder serverInstallOption, OptionSpecBuilder extractOption, OptionSpecBuilder helpOption) throws IOException
	    {
	        String path = VersionInfo.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	        if (path.contains("!/"))
	        {
	            System.out.println("Due to java limitation, please do not run this jar in a folder ending with !");
	            System.out.println(path);
	            return;
	        }

	        if (optionSet.has(serverInstallOption))
	        {
	            try
	            {
	                VersionInfo.getVersionTarget();
	                ServerInstall.headless = true;
	                System.out.println("Installing server to current directory");
	                if (!InstallerAction.SERVER.run(new File(".")))
	                {
	                    System.err.println("There was an error during server installation");
	                    System.exit(1);
	                }
	                else
	                {
	                    System.out.println("The server installed successfully, you should now be able to run the file "+VersionInfo.getContainedFile());
	                    System.out.println("You can delete this installer file now if you wish");
	                }
	                System.exit(0);
	            }
	            catch (Throwable e)
	            {
	                System.err.println("A problem installing the server was detected, server install cannot continue");
	                System.exit(1);
	            }
	        }
	        else if (optionSet.has(extractOption))
	        {
	            try
	            {
	                VersionInfo.getVersionTarget();
	                if (!InstallerAction.EXTRACT.run(new File(".")))
	                {
	                    System.err.println("A problem occurred extracting the file to "+VersionInfo.getContainedFile());
	                    System.exit(1);
	                }
	                else
	                {
	                    System.out.println("File extracted successfully to "+VersionInfo.getContainedFile());
	                    System.out.println("You can delete this installer file now if you wish");
	                }
	                System.exit(0);
	            }
	            catch (Throwable e)
	            {
	                System.err.println("A problem extracting the file was detected, extraction failed");
	                System.exit(1);
	            }
	        }
	        else
	        {
	            parser.printHelpOn(System.err);
	        }
	    }
		
		private static class MultiOutputStream extends OutputStream
	    {
	        OutputStream[] outs;
	        MultiOutputStream(OutputStream... outs)
	        {
	            this.outs = outs;
	        }

	        @Override
	        public void write(int b) throws IOException
	        {
	            for (OutputStream out : outs)
	                out.write(b);
	        }

	        @Override
	        public void write(byte b[]) throws IOException
	        {
	            for (OutputStream out : outs)
	                out.write(b);
	        }

	        @Override
	        public void write(byte b[], int off, int len) throws IOException
	        {
	            for (OutputStream out : outs)
	                out.write(b, off, len);
	        }

	        @Override
	        public void flush() throws IOException
	        {
	            for (OutputStream out : outs)
	                out.flush();
	        }

	        @Override
	        public void close() throws IOException
	        {
	            for (OutputStream out : outs)
	                out.close();
	        }
	    }
}
