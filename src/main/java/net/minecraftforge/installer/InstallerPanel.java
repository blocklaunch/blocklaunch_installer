package net.minecraftforge.installer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.LineBorder;

import org.blocklaunch.Mod;
import org.blocklaunch.ModPack;
import org.blocklaunch.skin.JModCheckBox;

import com.google.common.base.Throwables;

public class InstallerPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private Frame emptyFrame;
    private JOptionPane optionPane;
    
    private File targetDir;
    private ButtonGroup choiceButtonGroup;
    private JTextField selectedDirText;
    private JLabel infoLabel;
    private JButton sponsorButton;
    private JDialog dialog;
    //private JLabel sponsorLogo;
    private JPanel sponsorPanel;
    private JPanel fileEntryPanel;
    private JPanel modifyPanel;
    

    private class FileSelectAction extends AbstractAction
    {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e)
        {
            JFileChooser dirChooser = new JFileChooser();
            dirChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            dirChooser.setFileHidingEnabled(false);
            dirChooser.ensureFileIsVisible(targetDir);
            dirChooser.setSelectedFile(targetDir);
            int response = dirChooser.showOpenDialog(InstallerPanel.this);
            switch (response)
            {
            case JFileChooser.APPROVE_OPTION:
                targetDir = dirChooser.getSelectedFile();
                updateFilePath();
                break;
            default:
                break;
            }
        }
    }

    private class SelectButtonAction extends AbstractAction
    {
        private static final long serialVersionUID = 1L;

        @Override
        public void actionPerformed(ActionEvent e)
        {
            updateFilePath();
        }

    }
    public InstallerPanel(File targetDir)
    {	BufferedImage image;
        try
        {
            image = ImageIO.read(SimpleInstaller.class.getResourceAsStream("/org/blocklaunch/skin/Logo.png"));
        }
        catch (IOException e)
        {
            throw Throwables.propagate(e);
        }
    	ImageIcon icon = new ImageIcon(image);
    	
    	GridBagLayout gridBagLayout = new GridBagLayout();
    	System.out.println(icon.getIconWidth());
		gridBagLayout.columnWidths = new int[]{icon.getIconWidth()/4, 2*icon.getIconWidth()/4 , icon.getIconWidth()/4};
		gridBagLayout.rowHeights = new int[]{icon.getIconHeight()+10, 33, 103, 33};
		gridBagLayout.location(0, 0);
//		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0};
//		gridBagLayout.rowWeights = new double[]{2.0, 1.0, 3.0, 1.0,  Double.MIN_VALUE};
		
        this.setLayout(gridBagLayout);
        
        

        JPanel logoSplash = new JPanel();
        logoSplash.setLayout(new BoxLayout(logoSplash, BoxLayout.Y_AXIS));
        //ImageIcon icon = new ImageIcon(image);
        JLabel logoLabel = new JLabel(icon);
        logoLabel.setAlignmentX(CENTER_ALIGNMENT);
        logoLabel.setAlignmentY(CENTER_ALIGNMENT);
        logoLabel.setSize(image.getWidth(), image.getHeight());
        logoSplash.add(logoLabel);
//        JLabel tag = new JLabel(VersionInfo.getWelcomeMessage());
//        tag.setAlignmentX(CENTER_ALIGNMENT);
//        tag.setAlignmentY(CENTER_ALIGNMENT);
//        logoSplash.add(tag);
//        tag = new JLabel(VersionInfo.getVersion());
//        tag.setAlignmentX(CENTER_ALIGNMENT);
//        tag.setAlignmentY(CENTER_ALIGNMENT);
//        logoSplash.add(tag);
//        logoSplash.setAlignmentX(CENTER_ALIGNMENT);
//        logoSplash.setAlignmentY(TOP_ALIGNMENT);
        addToPane(logoSplash, 0, 0, 3, GridBagConstraints.BOTH);

        sponsorPanel = new JPanel();
        sponsorPanel.setLayout(new BoxLayout(sponsorPanel, BoxLayout.X_AXIS));
        sponsorPanel.setAlignmentX(CENTER_ALIGNMENT);
        sponsorPanel.setAlignmentY(CENTER_ALIGNMENT);

//        sponsorLogo = new JLabel();
//        sponsorLogo.setSize(50, 20);
//        sponsorLogo.setAlignmentX(CENTER_ALIGNMENT);
//        sponsorLogo.setAlignmentY(CENTER_ALIGNMENT);
//        sponsorPanel.add(sponsorLogo);

        sponsorButton = new JButton();
//        sponsorButton.setAlignmentX(CENTER_ALIGNMENT);
//        sponsorButton.setAlignmentY(CENTER_ALIGNMENT);
        sponsorButton.setBorderPainted(false);
        sponsorButton.setOpaque(false);
        sponsorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    Desktop.getDesktop().browse(new URI(sponsorButton.getToolTipText()));
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run()
                        {
                            InstallerPanel.this.dialog.toFront();
                            InstallerPanel.this.dialog.requestFocus();
                        }
                    });
                }
                catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(InstallerPanel.this, "An error occurred launching the browser", "Error launching browser", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        sponsorPanel.add(sponsorButton);

        addToPane(sponsorPanel, 1, 1, GridBagConstraints.BOTH);

//        JPanel entryPanel = new JPanel();
//        entryPanel.setLayout(new BoxLayout(entryPanel,BoxLayout.X_AXIS));
        

        choiceButtonGroup = new ButtonGroup();

        JPanel choicePanel = new JPanel();
        choicePanel.setLayout(new BoxLayout(choicePanel, BoxLayout.Y_AXIS));
        boolean first = true;
        SelectButtonAction sba = new SelectButtonAction();
        for (InstallerAction action : InstallerAction.values())
        {
            if (action == InstallerAction.CLIENT && VersionInfo.hideClient()) continue;
            if (action == InstallerAction.SERVER && VersionInfo.hideServer()) continue;
            JRadioButton radioButton = new JRadioButton();
            radioButton.setAction(sba);
            radioButton.setText(action.getButtonLabel());
            radioButton.setActionCommand(action.name());
            radioButton.setToolTipText(action.getTooltip());
            radioButton.setSelected(first);
//            radioButton.setAlignmentX(LEFT_ALIGNMENT);
//            radioButton.setAlignmentY(CENTER_ALIGNMENT);
            choiceButtonGroup.add(radioButton);
            choicePanel.add(radioButton);
            first = false;
        }

//        choicePanel.setAlignmentX(CENTER_ALIGNMENT);
//        choicePanel.setAlignmentY(CENTER_ALIGNMENT);
        addToPane(choicePanel, 0, 2, GridBagConstraints.BOTH);
        
        //mod
        modifyPanel = new JPanel();
        
        JLabel lblmodify = new JLabel("Modify BlockLaunch");
		modifyPanel.add(lblmodify);
	
        modifyPanel.setLayout(new ScrollPaneLayout());//new BoxLayout(modifyPanel, BoxLayout.Y_AXIS));
        
        JLabel modify = new JLabel("Modify BlockLaunch Pack");
        modifyPanel.add(modify);
        //modifyPanel.setLayout(new BoxLayout(modifyPanel,BoxLayout.Y_AXIS));
        modifyPanel.add(Box.createVerticalGlue());
        Mod[] modlist = ModPack.modpack.getModlist();
        for(int i = 0; i<modlist.length; i++){
        	JModCheckBox modcheck = new JModCheckBox(modlist[i]);        	
//        	modcheck.setAlignmentX(LEFT_ALIGNMENT);
//        	modcheck.setAlignmentY(CENTER_ALIGNMENT);
        	modifyPanel.add(modcheck);        	
        }
        
        JScrollPane modifyScrollPane = new JScrollPane(modifyPanel);

        addToPane(this, modifyScrollPane, 1, 2, 2, 1, GridBagConstraints.NONE);
//        System.out.println("dom");
        JPanel entryPanel = new JPanel();
        entryPanel.setLayout(new BoxLayout(entryPanel,BoxLayout.X_AXIS));

        this.targetDir = targetDir;
        selectedDirText = new JTextField();
        selectedDirText.setEditable(false);
        selectedDirText.setToolTipText("Path to minecraft");
        selectedDirText.setColumns(30);
//        homeDir.setMaximumSize(homeDir.getPreferredSize());
        entryPanel.add(selectedDirText);
        JButton dirSelect = new JButton();
        dirSelect.setAction(new FileSelectAction());
        dirSelect.setText("...");
        dirSelect.setToolTipText("Select an alternative minecraft directory");
        entryPanel.add(dirSelect);

//        entryPanel.setAlignmentX(LEFT_ALIGNMENT);
//        entryPanel.setAlignmentY(TOP_ALIGNMENT);
        infoLabel = new JLabel();
        infoLabel.setHorizontalTextPosition(JLabel.LEFT);
        infoLabel.setVerticalTextPosition(JLabel.TOP);
        infoLabel.setAlignmentX(LEFT_ALIGNMENT);
        infoLabel.setAlignmentY(TOP_ALIGNMENT);
        infoLabel.setForeground(Color.RED);
        infoLabel.setVisible(false);

        fileEntryPanel = new JPanel();
        fileEntryPanel.setLayout(new BoxLayout(fileEntryPanel,BoxLayout.Y_AXIS));
        fileEntryPanel.add(infoLabel);
        fileEntryPanel.add(Box.createVerticalGlue());
        fileEntryPanel.add(entryPanel);
        fileEntryPanel.setAlignmentX(CENTER_ALIGNMENT);
        fileEntryPanel.setAlignmentY(TOP_ALIGNMENT);
        addToPane(fileEntryPanel, 0, 3, 3, GridBagConstraints.HORIZONTAL);
        

        
//        modifyButton = new JButton("Modify");
//        modifyButton.setAlignmentX(CENTER_ALIGNMENT);
//        modifyButton.setAlignmentY(CENTER_ALIGNMENT);
//        modifyButton.addActionListener(new ActionListener(){
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				
//				ModPackModfierDialog modifydialog = new ModPackModfierDialog();
////				optionPane.createDialog(new ModPackModfierDialog(), "ModPack Modifier");
////				modifydialog.add(new ModPackModfierDialog());
//				modifydialog.setVisible(true);
//				//modifydialog.setAutoRequestFocus(true);
////				modifydialog.setModalityType(Dialog.ModalityType.TOOLKIT_MODAL);
//			}        	
//        });
//        addToPane(modifyButton);
        
        updateFilePath();
    }

    
    private void addToPane(Component component, int gridx, int gridy, int fill){
    	addToPane(this, component, gridx, gridy, 1,1, fill);
    }        
    private void addToPane(Component component, int gridx, int gridy, int gridwidth, int fill){
    	addToPane(this, component, gridx, gridy, gridwidth,1, fill);
    }    	    
    private void addToPane(JPanel pane, Component component, int gridx, int gridy, int gridwidth, int gridheight, int fill){
    	GridBagConstraints c = new GridBagConstraints();
    	c.gridx = gridx;
    	c.gridy = gridy;
    	c.gridwidth = gridwidth;
    	c.gridheight = gridheight;
    	c.anchor = GridBagConstraints.NORTHWEST;
    	c.fill = fill;
    	c.ipady = 2;
    	c.insets = new Insets(0,2,0,2);
    	pane.add(component, c);
    }

    private void updateFilePath()
    {
        try
        {
            targetDir = targetDir.getCanonicalFile();
            selectedDirText.setText(targetDir.getPath());
        }
        catch (IOException e)
        {

        }

        InstallerAction action = InstallerAction.valueOf(choiceButtonGroup.getSelection().getActionCommand());
        boolean valid = action.isPathValid(targetDir);

        String sponsorMessage = action.getSponsorMessage();
        if (sponsorMessage != null)
        {
            sponsorButton.setText(sponsorMessage);
            sponsorButton.setToolTipText(action.getSponsorURL());
            if (action.getSponsorLogo() != null)
            {
                sponsorButton.setIcon(action.getSponsorLogo());
            }
            else
            {
                sponsorButton.setIcon(null);
            }
            sponsorPanel.setVisible(true);
        }
        else
        {
            sponsorPanel.setVisible(false);
        }
        if (valid)
        {
            selectedDirText.setForeground(Color.BLACK);
            infoLabel.setVisible(false);
            fileEntryPanel.setBorder(null);
        }
        else
        {
            selectedDirText.setForeground(Color.RED);
            fileEntryPanel.setBorder(new LineBorder(Color.RED));
            infoLabel.setText("<html>"+action.getFileError(targetDir)+"</html>");
            infoLabel.setVisible(true);
        }
        if (dialog!=null)
        {
            dialog.invalidate();
            dialog.setSize(dialog.getPreferredSize());
            dialog.pack();
        }
    }

    public void run()
    {
        optionPane = new JOptionPane(this, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);

        emptyFrame = new Frame("Mod system installer");
        emptyFrame.setUndecorated(true);
        emptyFrame.setVisible(true);
        emptyFrame.setLocationRelativeTo(null);
        emptyFrame.setModalExclusionType(Dialog.ModalExclusionType.NO_EXCLUDE);
        dialog = optionPane.createDialog(emptyFrame, "Mod system installer");
        dialog.setSize(640, dialog.getPreferredSize().height);
       // dialog.setSize(610, 500);
        dialog.setResizable(true);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setVisible(true);
        dialog.setModal(false);
        dialog.setModalExclusionType(Dialog.ModalExclusionType.TOOLKIT_EXCLUDE);
        int result = (Integer) (optionPane.getValue() != null ? optionPane.getValue() : -1);
        if (result == JOptionPane.OK_OPTION)
        {
            InstallerAction action = InstallerAction.valueOf(choiceButtonGroup.getSelection().getActionCommand());
            if (action.run(targetDir))
            {
                JOptionPane.showMessageDialog(null, action.getSuccessMessage(), "Complete", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        dialog.dispose();
        emptyFrame.dispose();
    }
}
